import datetime

from django.utils.timezone import get_current_timezone
from django.db import models
from django.urls import reverse
from django.utils import timezone


class Serviciu(models.Model):
    serviciu = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "servicii"

    def __str__(self):
        return self.serviciu

    def get_absolute_url(self):
        return reverse('serviciu-detail', kwargs={'pk': self.pk})


class Client(models.Model):
    nume = models.CharField(max_length=50)
    prenume = models.CharField(max_length=50)
    servicii = models.ManyToManyField(Serviciu)
    data_ora = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name_plural = "clienti"

    def __str__(self):
        if (type(self.data_ora) != str):
            return self.nume.capitalize() + " " + self.prenume.capitalize() + \
                   " - " + self.data_ora.strftime("%Y-%m-%d %H:%M")
        else:
            return self.nume.capitalize() + " " + self.prenume.capitalize() + \
                   " - " + self.data_ora

    def get_absolute_url(self):
        return reverse('clienti:clienti')

    def programat_azi(self):
        return ((self.data_ora <= timezone.now() + datetime.timedelta(days=1)) and
                (self.data_ora >= timezone.now()))

    def get_date(self):
        "Returns the client's appointment date."
        return '%s' % (self.data_ora)

    def get_full_name(self):
        "Returns the client's full name."
        return '%s:%s %s' % (self.pk, self.nume.capitalize(), self.prenume.capitalize())
