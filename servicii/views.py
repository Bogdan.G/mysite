from django.http import HttpResponseRedirect
from django.shortcuts import render
from homepage.models import Serviciu
from homepage.forms import ServiciuForm
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import DeleteView


@login_required()
def servicii(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ServiciuForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            form.save()

        return HttpResponseRedirect('')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ServiciuForm()
        servicii = Serviciu.objects.all()

    return render(request, 'servicii/serviciu_form.html',
                  {'form': form, 'servicii': servicii})


class ServiciuDelete(DeleteView):
    model = Serviciu
    success_url = reverse_lazy('servicii:adauga_servicii')
