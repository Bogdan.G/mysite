from django import forms
from django.forms import CheckboxSelectMultiple

from .models import Serviciu, Client


class ServiciuForm(forms.ModelForm):

    class Meta:
        model = Serviciu
        fields = ['serviciu']


class ClientForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = ['nume', 'prenume', 'servicii', 'data_ora']
        widgets = {
            'servicii': CheckboxSelectMultiple(),
            # 'data_ora': DateTimePicker(),
        }
