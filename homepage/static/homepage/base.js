$(document).ready(function () {
    var csrfTk = document.querySelector("#csrf-token");
    $(".nav li").on("click", function () {
        $(".nav li").removeClass("active");
        $(this).addClass("active");
    });
    // page is now ready, initialize the calendar...
    let url = "http://127.0.0.1:8000/clienti/client/";
    let urlPost = "";

    $("#calendar").fullCalendar({
        // put your options and callbacks here
        defaultView: 'agendaWeek',
        themeSystem: 'bootstrap3',
        header: {
            left: 'prev, next, today',
            center: 'title',
            right: 'month, agendaWeek, agendaDay'
        },
        editable: true,
        forceEventDuration: true,
        defaultTimedEventDuration: '00:30:00',

        eventClick: function (calEvent, jsEvent, view) {

            // alert('Event: ' + calEvent.clientId);
            // updateUrl(calEvent.clientId);
            urlPost = url + calEvent.clientId.toString() + "/";
            urlPost = urlPost.replace(/\s/, "");
            window.open(urlPost, "_self");

        },

        eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
            var evtStart = event.start._i;
            var year = evtStart[0];
            var month = ('0' + (evtStart[1] + 1)).slice(-2);
            var day = ('0' + evtStart[2]).slice(-2);
            var hour = ('0' + evtStart[3]).slice(-2);
            var minute = ('0' + evtStart[4]).slice(-2);
            var second = ('0' + evtStart[5]).slice(-2);
            var evtFormated = year + '-' + month + '-' + day +
                ' ' + hour + ':' + minute + ':' + second;
            console.log(evtFormated);
            var csrf_token = csrfTk.getElementsByTagName("input")[0]["defaultValue"];
            console.log(csrf_token);
            console.log('csrf value:' + $("#csrf-token").html());
            
            var cId = event.clientId;
            console.log(cId);

            function csrfSafeMethod(method) {
                // these HTTP methods do not require CSRF protection
                return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
            }
            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrf_token);
                    }
                }
            });

            $.ajax({
                url: 'http://127.0.0.1:8000/homepage/update-time/',
                type: 'POST',
                data: {
                    client_time: evtFormated,
                    client_id: cId
                },
                dataType: 'json'
            });

        },

        eventMouseover: function (calEvent, jsEvent, view) {

            // change the border color just for fun
            $(this).css("cursor", "pointer");

        }

    });

    let reg = new RegExp('&#39;', 'g');
    clienti_js.replace(reg, "");
    let dates = clienti_js.split(",");
    dates.pop();
    let clientiFullName = [];
    clientiFullName = clienti_full_name.split(",");
    clientiFullName.pop();
    let splitArr = [];
    for (let i = 0; i < clientiFullName.length; i++) {
        splitArr.push(clientiFullName[i].split(":"));
    }

    arrSource = {
        events: [
        ]
    };

    for (let i = 0; i < splitArr.length; i++) {
        arrSource.events.push({ title: splitArr[i][1], start: dates[i], clientId: splitArr[i][0] });
    }
    arrSource.events.push({ title: "third event" });

    $("#calendar").fullCalendar("addEventSource", arrSource);

    $("#btnAdd").click(function () {
        $("#calendar").replaceWith($("#client-add"));
        $("#client-add").css("display", "block");
        $("#btnAdd").css("display", "none");
    });

    $("#btn-back").click(function () {
        window.location.href = "http://127.0.0.1:8000/homepage/";
    });

});
