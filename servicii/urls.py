from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'servicii/(?P<pk>[0-9]+)/delete/$', views.ServiciuDelete.as_view(), name='serviciu-delete'),
    url(r'', views.servicii, name='adauga_servicii'),
]
