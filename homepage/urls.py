from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^search-results/$', views.search_results, name="search-results"),
    url(r'^update-time/$', views.change_client_time, name="change_client_time"),
    url(r'', views.adauga_client, name='homepage')
]
