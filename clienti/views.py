from django.shortcuts import render
from django.views.generic import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required

from homepage.forms import ClientForm
from homepage.models import Client, Serviciu


@login_required()
def servicii_clienti(request):
    servicii = Serviciu.objects.all()
    return render(request, 'clienti/clienti.html', {'servicii': servicii})


class ClientCreate(CreateView):
    model = Client
    template_name = 'clienti/client_form.html'
    form_class = ClientForm


class ClientUpdate(UpdateView):
    model = Client
    template_name = 'clienti/client_form.html'
    form_class = ClientForm


class ClientDelete(DeleteView):
    model = Client
    success_url = reverse_lazy('clienti:clienti')
