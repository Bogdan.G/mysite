from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.db.models import Q

from .forms import ClientForm
from .models import Client, Serviciu


def intro(request):
    return render(request, 'homepage/client_add.html')


@login_required
def adauga_client(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ClientForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            print('aadsfasdfa')
            print('the value is --->' + form['data_ora'].value())
            form.save()

        return HttpResponseRedirect('homepage/client_add.html')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ClientForm()
        clienti = Client.objects.all()
        print("a")
        clienti_date = ""
        for client in clienti:
            clienti_date += client.get_date() + ", "

        clienti_full_name = ""

        for client in clienti:
            clienti_full_name += client.get_full_name() + ", "

        return render(request, 'homepage/client_add.html',
                      {'form': form,
                       'clienti': clienti,
                       'clienti_date': clienti_date,
                       'clienti_full_name': clienti_full_name})


def search_results(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']

        nume_prenume = Q(nume__icontains=q) | Q(prenume__icontains=q)
        clienti = Client.objects.filter(nume_prenume)
        servicii = Serviciu.objects.filter(serviciu__icontains=q)
        return render(request, 'homepage/search_results.html',
                      {'clienti': clienti, 'servicii': servicii, 'query': q})
    else:
        return HttpResponse('Va rugam sa introduceti un termen de cautare')


def change_client_time(request):

    if request.is_ajax():
        client = Client.objects.get(pk=request.POST.get('client_id'))
        client.data_ora = request.POST.get('client_time')
        client.save()

    return HttpResponse(client.data_ora)
