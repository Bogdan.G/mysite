from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'client/add/$', views.ClientCreate.as_view(), name='client-create'),
    url(r'client/(?P<pk>[0-9]+)/$', views.ClientUpdate.as_view(), name='client-update'),
    url(r'client/(?P<pk>[0-9]+)/delete/$', views.ClientDelete.as_view(), name='client-delete'),
    url(r'', views.servicii_clienti, name='clienti'),
]
