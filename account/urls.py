from django.conf.urls import url
from django.contrib.auth import views


urlpatterns = [
    # previous login view
    # url(r'^login/$', views.user_login, name='login'),

    # login / logout urls

    url(r'^logout/$', views.logout, name='logout'),
    url(r'^logout-then-login/$', views.logout_then_login,
        name='logout_then_login'),
    url(r'', views.login, name='login'),
]
