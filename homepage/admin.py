from django.contrib import admin
from .models import Serviciu, Client

admin.site.register(Serviciu)
admin.site.register(Client)
